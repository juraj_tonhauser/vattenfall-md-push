package com.dbschenker.ariba.uploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.dbschenker.ariba.constants.Constants;
import com.dbschenker.ariba.nonpersistens.entity.CommonUser;
import com.dbschenker.ariba.nonpersistens.entity.NewPartitionedUser;
import com.dbschenker.ariba.nonpersistens.entity.PartitionedUser;
import com.dbschenker.ariba.tools.CSVLoader;
import com.dbschenker.ariba.tools.CSVWriter;

import lombok.extern.slf4j.Slf4j;

/**
 * @author juraj t. All work and no play makes juraj a dull boy
 *
 */

@Slf4j
public class App

{

	/**
	 * main logic of the application.
	 * <li>gets all users from the Upstream</li>
	 * <li>gets all uses from the Downstream</li>
	 * <li>iterates over upstream users, trying to match each one with a
	 * downstream one</li>
	 * <li>if User doesn't exists, it's added to the file with new users for
	 * Downstream</li>
	 * <li>file is uploaded to the Downstream</li>
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		log.info("Starting the application");
		CSVLoader<CommonUser> commonUserLoader = new CSVLoader<CommonUser>(new FileInputStream(getCommonUsers()), CommonUser.class);
		CSVLoader<PartitionedUser> partitionedUserLoader = new CSVLoader<PartitionedUser>(new FileInputStream(getPartitionedUsers()), PartitionedUser.class);

		List<CommonUser> commonUsers = new ArrayList<CommonUser>();
		List<PartitionedUser> partitionedUsers = new ArrayList<PartitionedUser>();
		List<NewPartitionedUser> newPartitionedUsers = new ArrayList<NewPartitionedUser>();

		commonUsers = commonUserLoader.load();
		partitionedUsers = partitionedUserLoader.load();
		for (CommonUser commUser : commonUsers) {
			if (!existsInDownstream(commUser, partitionedUsers)) {
				newPartitionedUsers.add(new NewPartitionedUser(commUser));
				break;
			}
		}

		File zipFileWithNewUsers = writeUsersToFile(newPartitionedUsers);
		uploadToUrl(Constants.URL_PARAM_UPLOAD_CHILD_1, zipFileWithNewUsers);

		log.info("Execution finisheds");

	}

	/**
	 * 
	 * @param newPartitionedUsers
	 * @return
	 */
	private static File writeUsersToFile(List<NewPartitionedUser> newPartitionedUsers) {
		File newUsers = new File("UserConsolidated.csv");
		File zipFileWithNewUsers = new File("batchLoad.zip");
		CSVWriter<NewPartitionedUser> writer = new CSVWriter<NewPartitionedUser>(newUsers, NewPartitionedUser.class);
		try {
			writer.write((List<NewPartitionedUser>) newPartitionedUsers);
			FileInputStream in = new FileInputStream(newUsers);
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileWithNewUsers));
			out.putNextEntry(new ZipEntry("UserConsolidated.csv"));
			byte[] b = new byte[1024];
			int count;
			while ((count = in.read(b)) > 0) {
				out.write(b, 0, count);
			}
			out.close();
			in.close();
		} catch (IOException e) {
			log.error("issue with writing users into the file", e);
		}
		return zipFileWithNewUsers;
	}

	private static boolean existsInDownstream(CommonUser commUser, List<PartitionedUser> partitionedUsers) {
		boolean exists = false;
		for (PartitionedUser parUser : partitionedUsers) {
			if (commUser.getLoginId().equals(parUser.getUniqueName()) && parUser.getPasswordAdapter().equals(Constants.PASSWORDADAPTER_1)) {
				exists = true;
				break;
			}
		}
		log.info("user " + commUser.getLoginId() + " doesn't exist in Downstream ");
		return exists;
	}

	/**
	 * gets the csv file with the common users from Ariba Upstream
	 * 
	 * @return
	 */
	private static File getCommonUsers() {
		log.info("retrieving csv file with common Users from Ariba Upstream");
		ResponseEntity<byte[]> response = downloadFromURL(Constants.URL_PARAM_DOWNLOAD_UPSTREAM, Constants.EVENT_VALUE_EXPORT_COMMON_USERS_UPSTREAM);
		File newFile = unzipTheResponseFile(response, Constants.COMMON_USER_ZIP_FILENAME);
		log.info("Common Users file retrieved " + newFile.getAbsolutePath());
		return newFile;
	}

	/**
	 * gets the csv file with partitioned users from Ariba Downstream
	 * 
	 * @return
	 */
	private static File getPartitionedUsers() {
		log.info("retrieving csv file with partitioned Users from Ariba Downstream");
		ResponseEntity<byte[]> response = downloadFromURL(Constants.URL_PARAM_DOWNLOAD_CHILD_1, Constants.EVENT_VALUE_EXPORT_PARTITIONED_USER_DOWNSTREAM);
		File newFile = unzipTheResponseFile(response, Constants.PARTITIONED_USER_ZIP_FILENAME);
		log.info("Partitioned Users file retrieved " + newFile.getAbsolutePath());
		return newFile;
	}

	/**
	 * reads array of bytes retrieved from Ariba - which represents zipped csv
	 * file and unzip it
	 * 
	 * @param response
	 *            bytes retrieved from Ariba
	 * @param downloadedFileName
	 *            fileName to be used
	 * @return
	 */
	private static File unzipTheResponseFile(ResponseEntity<byte[]> response, String downloadedFileName) {		
		FileOutputStream output;
		ZipInputStream zis;
		File newFile = null;
		try {
			output = new FileOutputStream(new File(downloadedFileName));
			IOUtils.write(response.getBody(), output);
			zis = new ZipInputStream(new FileInputStream(downloadedFileName));
			ZipEntry ze = zis.getNextEntry();
			byte[] buffer = new byte[1024];
			while (ze != null) {
				String fileName = ze.getName();
				newFile = new File(Constants.TEMP_FOLDER + File.separator + fileName);
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
		} catch (FileNotFoundException e) {
			log.error("issue with unzipping the file with common users", e);
		} catch (IOException e) {
			log.error("issue with unzipping the file with common users", e);
		}
		return newFile;
	}

	/**
	 * 
	 * @param url
	 *            either way upstream or downstream url
	 * @param event
	 *            integration event name, must be visible in Ariba admin UI
	 *            under Integration Toolkit Name
	 * @return
	 */
	private static ResponseEntity<byte[]> downloadFromURL(String url, String event) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add(Constants.EVENT_PARAM, event);
		map.add(Constants.SHAREDSECRET_PARAM, Constants.SHAREDSECRET_VALUE);
		map.add(Constants.CLIENTTYPE_PARAM, Constants.CLIENTTYPE_VALUE);
		map.add(Constants.CLIENTINFO_PARAM, Constants.CLIENTINFO_VALUE);
		map.add(Constants.CLIENTVERSION_PARAM, Constants.CLIENTVERSION_VALUE);

		HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);
		ResponseEntity<byte[]> response = null;

		try {
			RestTemplate restTemplate;
			if (Constants.USE_PROXY) {
				SimpleClientHttpRequestFactory clientHttpReq = new SimpleClientHttpRequestFactory();
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constants.URL_PROXY, Integer.valueOf(Constants.PORT_PROXY)));
				clientHttpReq.setProxy(proxy);
				restTemplate = new RestTemplate(clientHttpReq);
			} else {
				restTemplate = new RestTemplate();
			}
			response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, byte[].class);
		} catch (Exception e) {
			log.error("issue in connecting to the " + url + " for event " + event, e);
		}
		return response;
	}

	/**
	 * upload file to the specified URL.
	 * 
	 * @param url
	 * @param f
	 */
	private static void uploadToUrl(String url, File f) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add(Constants.EVENT_PARAM, Constants.EVENT_VALUE_IMPORT);
		map.add(Constants.FULLLOAD_PARAM, Boolean.FALSE);
		map.add(Constants.CONTENT_PARAM, new FileSystemResource(f.getPath()));
		map.add(Constants.SHAREDSECRET_PARAM, Constants.SHAREDSECRET_VALUE);
		map.add(Constants.CLIENTTYPE_PARAM, Constants.CLIENTTYPE_VALUE);
		map.add(Constants.CLIENTINFO_PARAM, Constants.CLIENTINFO_VALUE);
		map.add(Constants.CLIENTVERSION_PARAM, Constants.CLIENTVERSION_VALUE);
		HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);
		ResponseEntity<String> response = null;
		try {
			RestTemplate restTemplate;
			if (Constants.USE_PROXY) {
				SimpleClientHttpRequestFactory clientHttpReq = new SimpleClientHttpRequestFactory();
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constants.URL_PROXY, Integer.valueOf(Constants.PORT_PROXY)));
				clientHttpReq.setProxy(proxy);
				restTemplate = new RestTemplate(clientHttpReq);
			} else {
				restTemplate = new RestTemplate();
			}

			response = restTemplate.exchange(Constants.URL_PARAM_UPLOAD_CHILD_1, HttpMethod.POST, requestEntity, String.class);
		} catch (Exception e) {
			log.error("issue in connecting to the " + url + " for upload event", e);
		}
		System.out.println(response.getStatusCodeValue());
	}
}
