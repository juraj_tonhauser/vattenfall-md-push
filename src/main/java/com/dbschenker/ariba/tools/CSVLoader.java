package com.dbschenker.ariba.tools;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Locale;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

/**
 * Reads the csv file into entities
 */
public class CSVLoader<T> {

	private final InputStream inputStream;
	private final Class<T> entityType;
	private final boolean shouldSkipFirstLine;

	public CSVLoader(InputStream inputStream, Class<T> entityType) throws IOException {
		this.entityType = entityType;
		final byte[] bytes = ByteStreams.toByteArray(inputStream);
		this.inputStream = new ByteArrayInputStream(bytes);
		shouldSkipFirstLine = shouldSkipFirstLine(new ByteArrayInputStream(bytes));
	}

	public List<T> load() throws IOException {
		return load(new AlwaysTrue());
	}

	public List<T> load(Predicate<T> predicate) throws IOException {

		Locale.setDefault(Locale.ENGLISH);
		final List<T> filteredRecords = Lists.newArrayList();

		if (skip()) {
			return Lists.newArrayList();
		}

		Reader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		if (shouldSkipFirstLine) {
			bufferedReader.readLine();
		}

		final BeanListProcessor<T> rowProcessor = new BeanListProcessor<T>(entityType);
		final CsvParserSettings settings = new CsvParserSettings();
		settings.setRowProcessor(rowProcessor);
		settings.setHeaderExtractionEnabled(true);
		final CsvParser parser = new CsvParser(settings);

		parser.parse(bufferedReader);
		bufferedReader.close();
		inputStreamReader.close();

		final List<T> records = rowProcessor.getBeans();

		filteredRecords.addAll(filter(records));

		return FluentIterable.from(filteredRecords).filter(predicate).toList();
	}

	private boolean shouldSkipFirstLine(InputStream inputStream) throws IOException {
		return true;
	}

	protected boolean skip() {
		return false;
	}

	protected List<T> filter(List<T> in) {
		return Lists.newArrayList(in);
	}

	private class AlwaysTrue implements Predicate<T> {
		@Override
		public boolean apply(T t) {
			return true;
		}
	}

}
